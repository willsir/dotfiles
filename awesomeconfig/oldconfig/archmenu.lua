 local menu98edb85b00d9527ad5acebe451b3fae6 = {
     {"Vim", "xterm -e vim ", "/usr/share/icons/hicolor/48x48/apps/gvim.png" },
 }

 local menuc8205c7636e728d448c2774e6a4a944b = {
     {"Avahi SSH Server Browser", "/usr/bin/bssh"},
     {"Avahi VNC Server Browser", "/usr/bin/bvnc"},
     {"Firefox", "/usr/lib/firefox/firefox ", "/usr/share/icons/hicolor/16x16/apps/firefox.png" },
     {"Telegram Desktop", "telegram-desktop -- ", "/usr/share/icons/hicolor/16x16/apps/telegram.png" },
 }

 local menu52dd1c847264a75f400961bfb4d1c849 = {
     {"Echomixer", "echomixer", "/usr/share/icons/hicolor/48x48/apps/echomixer.png" },
     {"Envy24 Control", "envy24control", "/usr/share/icons/hicolor/48x48/apps/envy24control.png" },
     {"HDAJackRetask", "hdajackretask"},
     {"HDSPConf", "hdspconf", "/usr/share/icons/hicolor/48x48/apps/hdspconf.png" },
     {"HDSPMixer", "hdspmixer", "/usr/share/icons/hicolor/48x48/apps/hdspmixer.png" },
     {"Hwmixvolume", "hwmixvolume", "/usr/share/icons/hicolor/48x48/apps/hwmixvolume.png" },
     {"Qt V4L2 test Utility", "qv4l2", "/usr/share/icons/hicolor/16x16/apps/qv4l2.png" },
     {"Qt V4L2 video capture utility", "qvidcap", "/usr/share/icons/hicolor/16x16/apps/qvidcap.png" },
     {"VLC media player", "/usr/bin/vlc --started-from-file ", "/usr/share/icons/hicolor/16x16/apps/vlc.png" },
 }

 local menuee69799670a33f75d45c57d1d1cd0ab3 = {
     {"Avahi Zeroconf Browser", "/usr/bin/avahi-discover"},
     {"LXTerminal", "lxterminal", "/usr/share/icons/hicolor/128x128/apps/lxterminal.png" },
     {"ranger", "xterm -e ranger"},
 }

xdgmenu = {
    {"Accessories", menu98edb85b00d9527ad5acebe451b3fae6},
    {"Internet", menuc8205c7636e728d448c2774e6a4a944b},
    {"Sound & Video", menu52dd1c847264a75f400961bfb4d1c849},
    {"System Tools", menuee69799670a33f75d45c57d1d1cd0ab3},
}
xdg_menu = require("archmenu")
mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
{ "Applications", xdgmenu },
{ "open terminal", terminal }
                                                                                                          }
                                                                                                  })
