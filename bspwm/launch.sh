#! /bin/sh

bspc rule -r "*"

# kill every process to prevent from being multiple process :)
killall -q polybar
killall -q sxhkd
killall -q xcmenu
killall -q feh
killall nm-applet
killall xfce4-power-manager
##############################################################
#--------------First Launching Apps--------------------------#
##############################################################
sleep 1;
sxhkd &
feh --bg-scale ~/.config/bspwm/wallpapers/bluehairgirl.jpg &
polybar bar1 >>/tmp/polybar1.log 2>&1 &
#polybar bar2 >>/tmp/polybar2.log 2>&1 &
sleep 1;
xcmenu --daemon &
nm-applet &
xfce4-power-manager &
picom --config ~/.config/picom/picom.conf &
